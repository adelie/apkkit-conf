=========
 APK Kit
=========
:Authors:
  * **A. Wilcox**
:Version:
  1.0
:Status:
  Alpha
:Copyright:
  © 2015-2016 Adélie Linux Team.  NCSA open source licence.


Purpose
=======

This repository contains the Adélie Linux /etc/apkkit directory as used on
our builders.
